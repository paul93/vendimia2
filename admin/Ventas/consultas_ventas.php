<section>
  <?php //Ejemplo aprenderaprogramar.com
         $nombre2 = $_POST['nombre'];
         $descripcion2 =$_POST['descripcion'];
         require("BD/conexion.php");
          //Consulta para obtener los datos del usuario de la tabla usuarios
          $sql = "SELECT clientes.id_cliente, clientes.nombre, clientes.apellido_paterno, clientes.apellido_materno, clientes.rfc, clientes.num_cliente,
          articulos.id_articulo, articulos.descripcion, articulos.modelo, articulos.precio, articulos.existencia, configuracion.id_configuracion,
          configuracion.financiamiento, configuracion.enganche, configuracion.plazo FROM clientes INNER JOIN articulos INNER JOIN configuracion where clientes.nombre = '$nombre2' AND articulos.descripcion = '$descripcion2'";
          $result = mysqli_query($con,$sql);
          while($row = mysqli_fetch_array($result)) {
            $id_cliente=$row["id_cliente"];
            $nombre=$row["nombre"];
            $apellido_paterno=$row["apellido_paterno"];
            $apellido_materno=$row["apellido_materno"];
            $rfc=$row["rfc"];
            $num_cliente=$row["num_cliente"];
            $id_articulo=$row["id_articulo"];
            $descripcion=$row["descripcion"];
            $modelo=$row["modelo"];
            $precio=$row["precio"];
            $existencia=$row["existencia"];
            $id_configuracion=$row["id_configuracion"];
            $financiamiento_configuracion=$row["financiamiento"];
            $enganche_configuracion=$row["enganche"];
            $plazo=$row["plazo"];
            }

            $sql2 = "SELECT ventas.id_venta, ventas.nombre_cliente, ventas.paterno_cliente,
            ventas.materno_cliente, ventas.descripcion_articulo, ventas.modelo_articulo, ventas.cantidad_articulo, ventas.precio_articulo, ventas.importe_articulo,
            ventas.enganche_ventas, ventas.bonificacion_enganche, ventas.total, ventas.folio_venta FROM ventas where ventas.nombre_cliente = '$nombre2' AND ventas.descripcion_articulo = '$descripcion2'";
            $result2 = mysqli_query($con,$sql2);
            while($row2 = mysqli_fetch_array($result2)) {
              $id_venta=$row2["id_venta"];
              $nombre_cliente=$row2["nombre_cliente"];
              $paterno_cliente=$row2["paterno_cliente"];
              $materno_cliente=$row2["materno_cliente"];
              $descripcion_articulo=$row2["descripcion_articulo"];
              $modelo_articulo=$row2["modelo_articulo"];
              $cantidad_articulo=$row2["cantidad_articulo"];
              $precio_articulo=$row2["precio_articulo"];
              $importe_articulo=$row2["importe_articulo"];
              $enganche=$row2["enganche_ventas"];
              $bonificacion_enganche=$row2["bonificacion_enganche"];
              $total=$row2["total"];
              $folio_venta=$row2["folio_venta"];

              }

            $nombre_cliente=$nombre;
            $paterno_cliente= $apellido_paterno;
            $materno_cliente=$apellido_materno;

            $descripcion_articulo=$descripcion2;
            $modelo_articulo= $modelo;
            $cantidad_articulo=$existencia;

            $precio_articulo = $precio*(1+($financiamiento_configuracion*12)/100);
            $importe_articulo = $precio_articulo * $cantidad_articulo;
            $enganche = ($enganche_configuracion/100)* $importe_articulo;
            $bonificacion_enganche = $enganche_configuracion *(($financiamiento_configuracion*12)/100);
            $total = $importe_articulo - $enganche_configuracion - $bonificacion_enganche;


  ?>
  <input type="hidden" id="nombre_cliente" name="nombre_cliente" value="<?php echo $nombre_cliente; ?>">
  <input type="hidden" id="paterno_cliente" name="paterno_cliente" value="<?php echo $paterno_cliente; ?>">
  <input type="hidden" id="materno_cliente" name="materno_cliente" value="<?php echo $materno_cliente; ?>">

  	<div class="section-body contain-lg">
		<div class="row">
			<div class="col-md-12">

								<div class="form card card-bordered style-primary">
									<div class="card-head">
										<div class="tools">
											<div class="btn-group">
												<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
												<a class="btn btn-icon-toggle btn-close" onclick="calcularVenta();"><i class="md md-close"></i></a>
											</div>
										</div>
										<header><i class="fa fa-fw fa-bar-chart"></i> Registro de ventas</header>
									</div>
									<div class="card-body style-default-bright">
										<div class="row ui-widget">
											<div class="col-sm-5">
												<div class="form-group">
													<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre2; ?>" readonly>
													<label for="nombre">Buscar cliente</label>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group">
													<input type="text" class="form-control" id="descripcion" name="descripcion" value="<?php echo $descripcion2; ?>" readonly>
													<label for="descripcion">Buscar articulo</label>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
														<button class="btn ink-reaction btn-floating-action btn-primary" onclick="calcularVenta();"><i class="fa md-save"></i></button>
												</div>
											</div>
										</div>
										<div class="row">

							        <div class="col-lg-12">
							          <div class="table-responsive">
							            <table class="table table-hover" style="border:1px solid #BFBFBF;">
							              <thead>
							                <tr>
							                  <th>Descripcion articulo</th>
							                  <th>Modelo</th>
							                  <th>Cantidad</th>
							                  <th>Precio</th>
							                  <th>Importe</th>
							                  <th class="text-right">Acciones</th>
							                </tr>
							              </thead>
							              <tbody>
							                <tr>
							                  <td><input type="text" class="form-control" value="<?php echo $descripcion_articulo; ?>" name="descripcion_articulo" id="descripcion_articulo" readonly></td>
							                  <td><input type="text" class="form-control" value="<?php echo $modelo_articulo; ?>" name="modelo_articulo" id="modelo_articulo" readonly></td>
                                <?php
                                   if(!empty($existencia)){
                                   ?>
                                <td>
                                <input type="text" class="form-control" name="existencia" id="existencia" onChange="removeExtraSpaces(this);" onkeypress="NumberKey(event);">
                                </td>
                                <?php
                                   }else{
                                   ?>
                                <td>
                                   <?php echo "El artículo seleccionado no cuenta con existencia, favor de verificar"; ?>
                                </td>
                                <?php
                                   }
                                   ?>
                                <td><input value="<?php echo $precio_articulo; ?>" <type="text" class="form-control" name="precio_articulo" id="precio_articulo" readonly></td>
																<td><input value="<?php echo $importe_articulo; ?>" <type="text" class="form-control" name="importe_articulo" id="importe_articulo" readonly></td>

							                  <td class="text-right">
                                <button type="button" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar" onclick="eliminarArticulo();"><i style="color: #E70505;" class="fa fa-trash-o"></i></button>
							                  </td>
							                </tr>

							              </tbody>
							            </table>
							          </div>
							        </div>
							      </div>

										<div class="row">
											<div class="col-sm-4">
												<div class="form-group">

												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">

												</div>
											</div>
											<div class="col-xs-4">
												<div class="well">
													<div class="clearfix">
														<div class="pull-left"> Enganche : </div>
														<div class="pull-right"><input value="<?php echo $enganche; ?>" <type="text" class="form-control" name="enganche" id="enganche" readonly></div>
													</div>
													<div class="clearfix">
														<div class="pull-left"> Bonificación enganche : </div>
														<div class="pull-right"><input value="<?php echo $bonificacion_enganche; ?>" <type="text" class="form-control" name="bonificacion_enganche" id="bonificacion_enganche" readonly></div>
													</div>
													<div class="clearfix">
														<div class="pull-left"> Total : </div>
														<div class="pull-right"><input value="<?php echo $total; ?>" <type="text" class="form-control" name="total" id="total" readonly></div>
													</div>
												</div>
										</div>

									</div>

								</div>

							</div>

						</div>

		</div>
	</div>


	</section>
